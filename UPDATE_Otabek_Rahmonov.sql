UPDATE film
SET rental_duration = 21, 
    rental_rate = 9.99
WHERE title = 'Your Favorite Film'; 

UPDATE customer
SET first_name = 'Your First Name',
    last_name = 'Your Last Name',
    address_id = (SELECT address_id FROM address LIMIT 1), 
    create_date = CURRENT_DATE
WHERE customer_id IN (
    SELECT customer_id
    FROM rental
    GROUP BY customer_id
    HAVING COUNT(*) >= 10
);

COMMIT;
